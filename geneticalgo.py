import random


class GeneticAlgorithm(object):
    def __init__(self):
        self.limit = 15

    def populate(self, obj: list) -> list:
        population = list()
        for o in obj:
            random_values = random.choices(range(2), k=len(obj))
            population.append(random_values)
        return population

    def set_limit(self, new_limit: int) -> int:
        self.limit = new_limit
