from item import ItemObject
from geneticalgo import GeneticAlgorithm
import random


def main():

    # Setting constant limit = 15
    LIMIT = 15

    # Creating object lists
    objects = [ItemObject(7, 5), ItemObject(
        2, 4), ItemObject(1, 7), ItemObject(9, 2)]

    # Initialize the Genetic Algorithm
    GA = GeneticAlgorithm()

    # Set weight limit to 15
    GA.set_limit(LIMIT)

    # Generate populations
    populations = GA.populate(objects)
    print(populations)


if __name__ == "__main__":
    main()
