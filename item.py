

class ItemObject():
    def __init__(self, weight, interest):
        '''
        Create an item object for the bag. Consists of weight and the interests values.
        '''
        self.weight = weight
        self.interest = interest
